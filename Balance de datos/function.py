from random import *
from Dataset import Dataset
from Dataset import DatasetSTD
from Dataset import DatasetReady

def leer(buff):#Primer parametro: ruta, segundo parametro: opciones
	if(len(buff)>1):
		print("leyendo "+buff[1])
		archivo=open(buff[1],'r')
		count=0#contador termporal
		d1=0
		d2=0
		lista=[]
		if(len(buff)>2):
			if buff[1]=="fromMatlab":
				for linea in archivo:
					buff2=linea.split(';')
					if len(buff2)>1:
						print(str(count)+";"+buff2[0]+";"+buff2[1]+";"+buff2[2]+";"+buff2[3]+";"+buff2[4]+";"+buff2[5]+";"+buff2[6]+";"+buff2[7]+";"+buff2[8])
						lista.append(DatasetSTD(count,buff2[0],buff2[1],buff2[2],buff2[3],buff2[4],buff2[5],buff2[6],buff2[7],buff2[8]))
						if bool(int(float(buff2[7])))==True:
							d1+=1
						if bool(int(float(buff2[8])))==True:
							d2+=1
						count+=1
			else:
				print("Comando no encontrado")
		else:
			for linea in archivo:
				re_fragmentacion=False
				if(linea[0:-1]=="\n"):
					linea=linea[0:-1]
				buff2=None
				buff2=linea.split()
				if len(buff2)==1:
					buff2=linea.split(";")
					re_fragmentacion=True
				if(re_fragmentacion==False):
					lista.append(DatasetReady(count,buff2[0],buff2[1],buff2[2],buff2[3],buff2[4],buff2[5],buff2[6],buff2[7]))
				else:
					lista.append(Dataset(count,float(buff2[0]),int(buff2[1]),int(buff2[2]),int(buff2[3]),int(buff2[4]),int(buff2[5]),int(buff2[6]),int(buff2[7])))
				if re_fragmentacion==False:
					if str(buff2[6][0])=="y":
						d1+=1
					if str(buff2[7][0])=="y":
						d2+=1
				else:
					if int(buff2[6])==0:
						d1+=1
					if int(buff2[7])==0:
						d2+=1
				count+=1
		print("+----------------------------+")
		print("|         Resumen            |")
		print("+----------------------------+")
		print("	Total: %s" %(count))
		print("	Positivos D1: %s" %(d1))
		print("	Negativos D1: %s" %(count-d1))
		print("	Positivos D2: %s" %(d2))
		print("	Negativos D2: %s" %(count-d2))
		print("+----------------------------+")
		archivo.close()
		return lista
	elif(len(buff)==1):
		print("Se necesita una ruta de archivo\n")
	else:
		print("Error desconocido: no se encontró comando asignado\n")

def balancear(lista):
	balancear=True
	buffClase1=[]#lista-buffer
	buffClase2=[]
	buffListPos=[]#lista de posiciones
	count=0;
	print("---Inicio de ciclo---")
	for obj in lista:
		if obj.getDx2()==1:
			buffClase1.append(obj)#lista de marca de clase 1
		else:
			buffClase2.append(obj)#lista de marca de clase 0
		count+=1
	print("---Fin de ciclo---")
	n_negativos=len(buffClase1)
	n_positivos=len(buffClase2)
	v_positiva=n_positivos-n_negativos
	v_negativa=n_negativos-n_positivos
	conjuntoAReducir=0;
	if v_negativa==v_positiva:
		print("El Dataset ya está balanceado")
		balancear=False
	if v_negativa>v_positiva:#si la variacion negativa es mayor a la positiva
		conjuntoAReducir=[v_negativa,1,buffClase1]
	else:
		conjuntoAReducir=[v_positiva,0,buffClase2]
	if balancear:
		print("Cantidad de coincidencias encontradas de 0: %s Con variacion de: %s" %(n_positivos,v_positiva))
		print("Cantidad de coincidencias encontradas de 1: %s Con variacion de: %s" %(n_negativos,v_negativa))
		print("Total: %s\n" %(count))
		print("Conclusion: reducir clase %s con diferencia de %s\n" %(str(conjuntoAReducir[1]),str(conjuntoAReducir[0])))
		for i in range(abs(v_negativa)):#misma variacion, pero solo se necesita su valor absoluto
			pos=randint(0,69)
			while(notRepeat(buffListPos,pos)==True):
				pos=randint(0,69)
			buffListPos.append(pos)
		for obj in buffListPos:
			buffClase1[obj].eliminated();
		buffer_str=[]
		for obj in buffClase1:
			if(int(obj.getID())>-1):
				buffer_str.append(str(obj.getTemperatura())+";"+str(obj.getNauseas())+";"+str(obj.getDolorLumbar())+";"+str(obj.getInsOrinar())+";"+str(obj.getDolorMiccional())+";"+str(obj.getArdorUretra())+";"+str(obj.getDx1())+";"+str(obj.getDx2()))
		for obj in buffClase2:
			buffer_str.append(str(obj.getTemperatura())+";"+str(obj.getNauseas())+";"+str(obj.getDolorLumbar())+";"+str(obj.getInsOrinar())+";"+str(obj.getDolorMiccional())+";"+str(obj.getArdorUretra())+";"+str(obj.getDx1())+";"+str(obj.getDx2()))
		#
		respuesta=input("¿Desea guardar los cambios? (s/n): ")
		if respuesta=='s' or respuesta=='si':
			archivo=open("out.txt","w")
			while(len(buffer_str)>0):
				pos=randint(0,len(buffer_str)-1)
				archivo.write(buffer_str[pos]+"\n")
				buffer_str.pop(pos);
			archivo.close()
			lista=[]
			archivo=open("out.txt","r")
			count=0
			for linea in archivo:
				linea=linea[0:-1]
				buff=linea.split(";")
				lista.append(Dataset(count,buff[0],buff[1],buff[2],buff[3],buff[4],buff[5],buff[6],buff[7]))
				count+=1
			archivo.close()
			print("Archivo Guardado")
	return lista

def notRepeat(lista,number):
	r=False
	if len(lista)>0:
		for i in lista:
			if i==number:
				r=True#verdadero ya que encontro coincidencia

		return r
def verDetalle(lista):
	count0=0
	count1=0
	count=0
	for obj in lista:
		if obj.getDx2()==0:
			count0+=1
		else:
			count1+=1
		count+=1
	print("+------------------------------+")
	print("|          Resumen             |")
	print("+------------------------------+")
	print("      Clase 0: "+str(count0))
	print("      Clase 1: "+str(count1))
	print("      Total:   "+str(count))
	print("+------------------------------+")

def verLista(lista):
	if len(lista)>0:
		for b in lista:
			print("["+str(b.getID())+","+str(b.getTemperatura())+","+str(b.getNauseas())+","+str(b.getDolorLumbar())+","+str(b.getInsOrinar())+","+str(b.getDolorMiccional())+","+str(b.getArdorUretra())+","+str(b.getDx1())+","+str(b.getDx2())+"]")
	else:
		print("Lista vacia\n")
def ayuda():
	print("+-------------------------------------------------------------------+")
	print("|                             Ayuda                                 |")
	print("+-----------+-------------------------------------------------------+")
	print("| Comando   |                     Descripcion                       |")
	print("+-----------+-------------------------------------------------------+")
	print("|salir      | salir de la aplicacion                                |")
	print("|leer       | leer desde archivo .txt [leer <archivo>.txt]          |")
	print("|ver lista  | ver lista cargada en memoria                          |")
	print("|ver detalle| ver detalle de lista en memoria                       |")
	print("|balancear  | balancear datos y reordenarlos aleatoriamente         |")
	print("+-------------------------------------------------------------------+")

def dividirDataset(lista):
	c_testing=open("test.txt","w")#20%
	c_training=open("training.txt","w")#80%
	count=0
	for obj in lista:
		if(count<20):
			c_testing.write(str(obj.getTemperatura())+";"+str(obj.getNauseas())+";"+str(obj.getDolorLumbar())+";"+str(obj.getInsOrinar())+";"+str(obj.getDolorMiccional())+";"+str(obj.getArdorUretra())+";"+str(obj.getDx1())+";"+str(obj.getDx2())+"\n")
		else:
			c_training.write(str(obj.getTemperatura())+";"+str(obj.getNauseas())+";"+str(obj.getDolorLumbar())+";"+str(obj.getInsOrinar())+";"+str(obj.getDolorMiccional())+";"+str(obj.getArdorUretra())+";"+str(obj.getDx1())+";"+str(obj.getDx2())+"\n")
		count+=1			
	c_testing.close()
	c_training.close()
	print("Division terminada")