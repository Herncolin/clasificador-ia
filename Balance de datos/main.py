from random import *
from Dataset import Dataset
from Dataset import DatasetSTD
from Dataset import DatasetReady
from function import *

#MAIN
print("+-------------------------------------------------------------------+")
print("|                      Clasificador diagnosta                       |")
print("+-------------------------------------------------------------------+\n")
ayuda()
while True:
	#atributos
	buff="";#buffer 1
	buff2="";#buffer 2
	#code
	res=input("\n>");
	buff=res.split(" ")
	if buff[0]=="salir":
		break
	elif buff[0]=="leer":
		lista=[]
		lista=leer(buff)
	elif buff[0]=="ver":
		if(len(buff)>1):
			if buff[1]=="lista":
				verLista(lista)
			elif buff[1]=="detalle":
				verDetalle(lista)
			else:
				print("Opcion no disponible\n")
	elif buff[0]=="balancear":
		lista=balancear(lista)
	elif buff[0]=="help" or buff[0]=="ayuda":
		ayuda()