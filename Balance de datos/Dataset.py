class Dataset:
	def __init__(self,ID,temperatura,nauseas,dolorLumbar,insOrinar,dolorMiccional,ardorUretra,Dx1,Dx2):
		self.__ID=ID
		self.__temperatura=temperatura
		self.__nauseas=nauseas
		self.__dolorLumbar=dolorLumbar
		self.__insOrinar=insOrinar
		self.__dolorMiccional=dolorMiccional
		self.__ardorUretra=ardorUretra
		self.__Dx1=Dx1
		self.__Dx2=Dx2
	def getTemperatura(self):
		return self.__temperatura
	def getNauseas(self):
		return self.__nauseas
	def getDolorLumbar(self):
		return self.__dolorLumbar
	def getInsOrinar(self):
		return self.__insOrinar
	def getDolorMiccional(self):
		return self.__dolorMiccional
	def getArdorUretra(self):
		return self.__ardorUretra
	def getDx1(self):
		return self.__Dx1
	def getDx2(self):
		return self.__Dx2
	def getID(self):
		return self.__ID
	def eliminated(self):
		self.__ID=-1
class DatasetSTD(Dataset):
	def __init__(self,ID,temperatura,temperaturaSTD,nauseas,dolorLumbar,insOrinar,dolorMiccional,ardorUretra,Dx1,Dx2):
		Dataset.__init__(self,ID,int(float(temperatura)),int(float(nauseas)),int(float(dolorLumbar)),int(float(insOrinar)),int(float(dolorMiccional)),int(float(ardorUretra)),int(float(Dx1)),int(float(Dx2)))
class DatasetReady(Dataset):
	def __init__(self,ID,temperatura,nauseas,dolorLumbar,insOrinar,dolorMiccional,ardorUretra,Dx1,Dx2):
		if(nauseas=="yes"):
			nauseas=0
		else:
			nauseas=1
		if(dolorLumbar=="yes"):
			dolorLumbar=0
		else:
			dolorLumbar=1
		if(insOrinar=="yes"):
			insOrinar=0
		else:
			insOrinar=1
		if(dolorMiccional=="yes"):
			dolorMiccional=0
		else:
			dolorMiccional=1
		if(ardorUretra=="yes"):
			ardorUretra=0
		else:
			ardorUretra=1
		if(Dx1=="yes"):
			Dx1=0
		else:
			Dx1=1
		if(Dx2=="yes"):
			Dx2=0
		else:
			Dx2=1
		Dataset.__init__(self,ID,float(temperatura),int((nauseas)),int((dolorLumbar)),int((insOrinar)),int((dolorMiccional)),int((ardorUretra)),int((Dx1)),int((Dx2)))
