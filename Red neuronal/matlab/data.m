classdef data
    properties
		temperatura;
		nauseas;
		dolorLumbar;
		insOrinar;
		dolorMiccional;
		ardorUretra;
		Dx1;%diagnostico 1
		Dx2;%diagnostico 2 (Marca de clase)
    end
    methods
        function nodo=data(temperatura,nauseas,dolorLumbar,insOrinar,dolorMiccional,ardorUretra,Dx1,Dx2)
            nodo.temperatura=temperatura;
            nodo.nauseas=nauseas;
            nodo.dolorLumbar=dolorLumbar;
            nodo.insOrinar=insOrinar;
            nodo.dolorMiccional=dolorMiccional;
            nodo.ardorUretra=ardorUretra;
            nodo.Dx1=Dx1;
            nodo.Dx2=Dx2;
        end
    end
end