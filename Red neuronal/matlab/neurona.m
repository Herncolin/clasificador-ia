classdef neurona<handle
    properties
        ID;%identificador referencial
        origen=[];%-1 origen desde los datos en crudo(capa de entrada)
        pesoOrigen=[];%-1 origen desde los datos en crudo(capa de entrada)
        nivel;%nivel en el que se encuentra
        dato;%datos guardado
        contador;
    end
    methods
        function nodo=neurona(ID,nivel,origen,pesoOrigen,n_datos)
            nodo.ID=ID;
            nodo.nivel=nivel;
            nodo.origen=origen;
            nodo.pesoOrigen=pesoOrigen;
            nodo.dato=zeros(n_datos);
            nodo.contador=1;
        end
        function entrada(nodo,pos,val)
            nodo.dato(pos)=sum(val*nodo.pesoOrigen(pos));
        end
    end
end