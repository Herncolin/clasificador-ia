clear
load dataset.txt;

tam=size(dataset);

%carga de datos a objeto
for i=1:tam(1)
    redData(i)=data(dataset(i,1),dataset(i,2),dataset(i,3),dataset(i,4),dataset(i,5),dataset(i,6),dataset(i,7),dataset(i,8));
end

%creacion de neuronas base
redNeuronal(1)=neurona(1,0,-1,-1,[tam(1) 1]); 
redNeuronal(2)=neurona(2,0,-1,-1,[tam(1) 1]); 
redNeuronal(3)=neurona(3,0,-1,-1,[tam(1) 1]);
redNeuronal(4)=neurona(4,0,-1,-1,[tam(1) 1]); 
redNeuronal(5)=neurona(5,0,-1,-1,[tam(1) 1]); 
redNeuronal(6)=neurona(6,0,-1,-1,[tam(1) 1]); 
redNeuronal(7)=neurona(7,0,-1,-1,[tam(1) 1]); 

%carga de datos a neuronas
for i=1:tam(1)
    redNeuronal(1).dato(i)=redData(i).temperatura;
    redNeuronal(2).dato(i)=redData(i).nauseas;
    redNeuronal(3).dato(i)=redData(i).dolorLumbar;
    redNeuronal(4).dato(i)=redData(i).insOrinar;
    redNeuronal(5).dato(i)=redData(i).dolorMiccional;
    redNeuronal(6).dato(i)=redData(i).ardorUretra;
    redNeuronal(7).dato(i)=redData(i).Dx1;
end

%capa 1 (oculta)
redNeuronal(8)=neurona(8,1,[1 2 3 4 5 6 7],[0.4 0.5 0.8 0.8 0.8 0.6 0.5],[tam(2)-1,1]);
redNeuronal(9)=neurona(9,1,[1 2 3 4 5 6 7],[0.4 0.5 0.8 0.8 0.8 0.6 0.5],[tam(2)-1,1]);

%Funcion de entrada (capa 1 oculta)
for i=1:tam(2)-1
    redNeuronal(8).entrada(i,redNeuronal(redNeuronal(8).origen(i)).dato);
    redNeuronal(9).entrada(i,redNeuronal(redNeuronal(9).origen(i)).dato);
end

%Funcion de activacion (capa 1 oculta)
redNeuronal(8)
redNeuronal(9)