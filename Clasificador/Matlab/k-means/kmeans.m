clear
format
load('dataset.txt');

centro=[0,0];
centro(1)=randi(99);
centro(2)=randi(99);
%el siguiente ciclo asegura que los 2 puntos aleatorios sean iguales
while 1
    if(centro(1)==centro(2))
        centro(2)=randi(99);
    else 
        break;
    end    
end

%Declaro los vectores de donde se almacenaran los indices que atraeran los
%respectivos centroides
centroide1=zeros(1,100);
centroide2=zeros(1,100);

j1=1;%contador centroide1
j2=1;%contador centroide2

for i=1:100
    r1=sqrt(sum((dataset(i,1:7)-dataset(centro(1),1:7)).^2));
    r2=sqrt(sum((dataset(i,1:7)-dataset(centro(2),1:7)).^2));
    if min(r1,r2)==r1
        centroide1(j1)=i;
        j1=j1+1;
    else
        centroide2(j2)=i;
        j2=j2+1;
    end
end

buff_centroide1=zeros(1,100);
buff_centroide2=zeros(1,100);

contador=0;
%ciclo principal de K-Means
while 1
    if(std(buff_centroide1==centroide1)==0 && std(buff_centroide2==centroide2)==0)%lo que pase primero
        break;
    else
        contador=contador+1;
        centro=zeros(2,7);
        centro(1,:)=[mean(dataset(centroide1(j1-1),1)),mean(dataset(centroide1(j1-1),2)),mean(dataset(centroide1(j1-1),3)),mean(dataset(centroide1(j1-1),4)),mean(dataset(centroide1(j1-1),5)),mean(dataset(centroide1(j1-1),6)),mean(dataset(centroide1(j1-1),7))];
        centro(2,:)=[mean(dataset(centroide2(j2-1),1)),mean(dataset(centroide2(j2-1),2)),mean(dataset(centroide2(j2-1),3)),mean(dataset(centroide2(j2-1),4)),mean(dataset(centroide2(j2-1),5)),mean(dataset(centroide2(j2-1),6)),mean(dataset(centroide2(j2-1),7))];
        j1=1;
        j2=1;
        buff_centroide1=centroide1;
        buff_centroide2=centroide2;
        centroide1=zeros(1,100);
        centroide2=zeros(1,100);
        for i=1:100
            r1=sqrt(sum((dataset(i,1:7)-centro(1,1:7)).^2));
            r2=sqrt(sum((dataset(i,1:7)-centro(2,1:7)).^2));
            if min(r1,r2)==r1
                centroide1(j1)=i;
                j1=j1+1;
            else
                centroide2(j2)=i;
                j2=j2+1;
            end
        end
    end
end

c1=fopen('conjunto1.txt','W');
c2=fopen('conjunto2.txt','W');
for i=1:100
    if centroide1(i)~=0
        fprintf(c1,'%i;%i;%i;%i;%i;%i;%i;%i\n',dataset(centroide1(i),1),dataset(centroide1(i),2),dataset(centroide1(i),3),dataset(centroide1(i),4),dataset(centroide1(i),5),dataset(centroide1(i),6),dataset(centroide1(i),7),dataset(centroide1(i),8));
    end
    if centroide2(i)~=0
        fprintf(c2,'%i;%i;%i;%i;%i;%i;%i;%i\n',dataset(centroide2(i),1),dataset(centroide2(i),2),dataset(centroide2(i),3),dataset(centroide2(i),4),dataset(centroide2(i),5),dataset(centroide2(i),6),dataset(centroide2(i),7),dataset(centroide2(i),8));
    end
end
fclose(c1);
fclose(c2);