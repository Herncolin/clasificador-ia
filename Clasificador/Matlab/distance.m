clear

dir='k-fold/5/';

load(strcat(dir,'training.txt'));
load(strcat(dir,'test.txt'));

n1=size(test);
respuestas=zeros(n1(1),2);

n_primeros=3;%numero de vecinos mas cercanos
buffer=zeros(1,n_primeros);%va a contener los primeros n elementos mas cercanos

for i=1:n1(1)
    r=sqrt(((training(:,1)-test(i,1)).^2)+((training(:,2)-test(i,2)).^2)+((training(:,3)-test(i,3)).^2)+((training(:,4)-test(i,4)).^2)+((training(:,5)-test(i,5)).^2)+((training(:,6)-test(i,6)).^2)+((training(:,7)-test(i,7)).^2));
    tmp=r;
    for j=1:n_primeros
        tmpV=find(tmp==min(tmp),1);
        tmpValue=tmp(tmpV);
        buffer(j)=find(r==tmpValue,1);
        tmp(tmpV)=[];
    end
    respuestas(i,1)=respuestas(i,1)+min(r);
    respuestas(i,2)=respuestas(i,2)+mode(training(buffer,8));
end

coincidencias=0;
reales_positivos=0;
reales_negativos=0;
falsos_positivos=0;
falsos_negativos=0;
tp=zeros(1,n1(1));%verdaderos positivos
fp=zeros(1,n1(1));%falsos positivos
for i=1:n1(1)
    if(respuestas(i,2)~=test(i,8))
        coincidencias=coincidencias+1;
        if(respuestas(i,2)==0 && test(i,8)==1)%Falso negativo
            falsos_negativos=falsos_negativos+1;
        elseif(respuestas(i,2)==1 && test(i,8)==0)%Falso positivo
            falsos_positivos=falsos_positivos+1;
        end
    else
        if(respuestas(i,2)==0 && test(i,8)==0)%Verdadero positivo
            reales_positivos=reales_positivos+1;
        elseif(respuestas(i,2)==1 && test(i,8)==1)%Verdadero negativo
            reales_negativos=reales_negativos+1;
        end
    end
end

coincidencias
reales_positivos
reales_negativos
falsos_positivos
falsos_negativos
asierto=((n1(1)-coincidencias)/n1(1))*100
sensibilidad=reales_positivos/(reales_positivos+falsos_negativos)
especifidad=reales_negativos/(reales_negativos+falsos_positivos)

fi=fopen(strcat(dir,'resultado.txt'),'W');
fprintf(fi,strcat('Numero de variaciones: ',int2str(coincidencias)));
fprintf(fi,strcat('\nReales positivos: ',int2str(reales_positivos)));
fprintf(fi,strcat('\nReales negativos: ',int2str(reales_negativos)));
fprintf(fi,strcat('\nFalsos positivos: ',int2str(falsos_negativos)));
fprintf(fi,strcat('\nFalsos negativos: ',int2str(falsos_positivos)));
fprintf(fi,strcat('\nTasa de asierto(percent): ',int2str(asierto)));
fprintf(fi,strcat('\nSensibilidad(percent): ',int2str(sensibilidad)));
fprintf(fi,strcat('\nEspecifidad(percent): ',int2str(especifidad)));
fclose(fi);