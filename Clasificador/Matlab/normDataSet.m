load out.txt
%
promedio=mean(out);
desviacion_std=std(out);
%
temperature_STD=(out(:,1)-promedio(1))/desviacion_std(1);
nauseas_STD=(out(:,2)-promedio(2))/desviacion_std(2);
dolorLumbar_STD=(out(:,3)-promedio(3))/desviacion_std(3);
insOrinar_STD=(out(:,4)-promedio(4))/desviacion_std(4);
dolorMiccional_STD=(out(:,5)-promedio(5))/desviacion_std(5);
ardorUretra_STD=(out(:,6)-promedio(6))/desviacion_std(6);
Dx1_STD=(out(:,7)-promedio(7))/desviacion_std(7);
%
v=randperm(100,100);
fi=fopen('salida.txt','w');
for i=1:100
    fprintf(fi,'%f;%f;%f;%f;%f;%f;%f;%i\n',temperature_STD(v(i)),nauseas_STD(v(i)),dolorLumbar_STD(v(i)),insOrinar_STD(v(i)),dolorMiccional_STD(v(i)),ardorUretra_STD(v(i)),Dx1_STD(v(i)),out(v(i),8));
end
fclose(fi);